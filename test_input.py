from pytest import raises
import pandas as pd
import os
from pathlib import Path


# run "pytest" on command line (maybe "pytest-3" on older computers)

# input 
input_dir = Path("input")
input_files = [input_dir / f for f in os.listdir(input_dir) if (input_dir / f).is_file() and (input_dir / f).name.lower() != "readme.txt"]

def test_one_input():
    """
    Input dir should only contain one file
    """
    assert len(input_files) == 1


def test_input_type():
    """
    Input should be a us xml file
    """
    names = [f.suffix for f in input_files]
    assert names[0] == ".xml"
